from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river


def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))

    # Sort the rivers into alphabetical order
    rivers = sorted(rivers_with_station(stations))

    # Print the total number of rivers with a minimum of 1 monitoring station
    print("Number of rivers with a minimum of one monitoring station =",
          len(rivers))

    # Print the first 10 in alphabetical order
    print(rivers[:10])

    # Creates a dictionary of rivers as keys with stations as values
    river_stations = stations_by_river(stations)

    # a = input("Enter river: ")
    for a in ("River Aire", "River Cam", "Thames"):
        # Empty list to hold station names
        station_list = []

        for station in river_stations[a]:
            station_list.append(station.name)
        print(a, ":", sorted(station_list))
        print()

if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")

    # Run Task1D
    run()
