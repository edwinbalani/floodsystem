from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_threshold


def run():
    """Requirements for Task 2B"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))
    update_water_levels(stations)
    print("Updated water levels for {} stations.".format(
            len([i for i in stations if i.latest_level is not None])))

    for s, level in stations_level_over_threshold(stations, 0.8):
        print(s.name, level)

if __name__ == "__main__":
    print("*** Task 2B: CUED Part IA Flood Warning System ***")

    # Run Task2B
    run()
