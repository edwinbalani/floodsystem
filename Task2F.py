from floodsystem.stationdata import build_station_list
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.datafetcher import fetch_measure_levels
import datetime
import matplotlib.pyplot as plt


def run():
    """Requirements for Task 2F"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))
    update_water_levels(stations)
    print("Updated water levels for {} stations.".format(
            len([i for i in stations if i.latest_level is not None])))

    # List to store top 5 stations with the highest water levels
    highest = stations_highest_rel_level(stations, 5)

    # Creating dates for the past 2 days and fetching water levels
    dt = 2
    for h in highest:
        dates, levels = fetch_measure_levels(h.measure_id,
                                             dt=datetime.timedelta(days=dt))
        # Add plot lines for the typical low and high water levels
        if h.typical_range_consistent():
            plt.axhline(y=h.typical_range[0], color='r', linestyle='-',
                        label='Lowest Typical Value')
            plt.axhline(y=h.typical_range[1], color='k', linestyle='-',
                        label='Highest Typical Value')
        else:
            raise ValueError('Typical range data is inconsistent')

        plot_water_level_with_fit(h, dates, levels, 4)

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")

    # Run Task2F
    run()
