from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level


def run():
    """Requirements for Task 2C"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))
    update_water_levels(stations)
    print("Updated water levels for {} stations.".format(
            len([i for i in stations if i.latest_level is not None])))

    for s in stations_highest_rel_level(stations, 10):
        print(s.name, s.relative_water_level())

if __name__ == "__main__":
    print("*** Task 2C: CUED Part IA Flood Warning System ***")

    # Run Task2C
    run()
