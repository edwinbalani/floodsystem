"""Unit test for floodsystem.geo"""
from floodsystem.station import MonitoringStation
import floodsystem.geo
from testing_utils import floatcheck
from collections import defaultdict


def prepare_stations():
    names = ["Pythonia Pool",
             "Spam City Weir",
             "Foobarton Locks",
             "Trumpington Bridge",
             "Lumbridge Central", ]

    coords = [(55.243215,  0.217564),
              (51.862437,  0.576200),
              (50.242350, -1.254632),
              (52.000000,  1.000000),
              (53.542500,  5.234873), ]
    rivers = ["Thames",
              "Trent",
              "Severn",
              "Ouse",
              "Ouse"]

    stations = []
    for i in range(min(len(names), len(coords), len(rivers))):
        stations.append(MonitoringStation(None, None, names[i], coords[i],
                                          None, rivers[i],
                                          names[i].split()[0]))
    return stations


def test_stations_by_distance():
    stations = prepare_stations()
    point = (52.234765, 0.276426)
    distances = {"Pythonia Pool": 334.546742040255,
                 "Spam City Weir": 46.1981896272176,
                 "Foobarton Locks": 245.839005151849,
                 "Trumpington Bridge": 55.877030957086,
                 "Lumbridge Central": 362.95934689859, }

    result = floodsystem.geo.stations_by_distance(stations, point)
    assert [s[0].name for s in result] == ["Spam City Weir",
                                           "Trumpington Bridge",
                                           "Foobarton Locks",
                                           "Pythonia Pool",
                                           "Lumbridge Central", ]
    for s in result:
        assert floatcheck(s[1], distances[s[0].name])


def test_stations_within_radius():
    stations = prepare_stations()
    orig_stations = stations[:]
    point = (52.234765, 0.276426)
    r = 250
    result = floodsystem.geo.stations_within_radius(stations, point, r)

    expected = stations[1:4]
    assert set(result) == set(expected)
    assert stations == orig_stations


def test_rivers_with_station():
    stations = prepare_stations()
    result = floodsystem.geo.rivers_with_station(stations)
    listofrivers = ["Thames", "Trent", "Severn", "Ouse"]
    correct = set(listofrivers)
    assert result == correct


def test_stations_by_river():
    stations = prepare_stations()
    result = floodsystem.geo.stations_by_river(stations)
    correct = {"Thames": [stations[0]],
               "Trent": [stations[1]],
               "Severn": [stations[2]],
               "Ouse": [stations[3], stations[4]]}
    assert result == correct


def test_rivers_by_station_number():
    N = 3
    stations = prepare_stations()
    result = floodsystem.geo.rivers_by_station_number(stations, N)

    assert len(result) >= N or len(result) == len(stations)

    for i in range(1, len(result)):
        assert result[i][1] <= result[i-1][1]

    d = defaultdict(set)
    for r in result:
        d[r[1]].add(r[0])
    assert d == {2: {"Ouse"}, 1: {"Thames", "Trent", "Severn"}}
