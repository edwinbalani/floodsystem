"""Helper functions for testing scripts"""


def floatcheck(test, actual, acc=5):
    """Checks that a test float value is equal to the actual value to
    specified accuracy (default 5)"""
    return round(test, acc) == round(actual, acc)
