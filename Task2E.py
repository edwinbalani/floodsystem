from floodsystem.stationdata import build_station_list
from floodsystem.flood import stations_highest_rel_level
from floodsystem.plot import plot_water_levels
from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import update_water_levels
import datetime


def run():
    """Requirements for Task 2E"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))
    update_water_levels(stations)
    print("Updated water levels for {} stations.".format(
            len([i for i in stations if i.latest_level is not None])))

    # List to store top 5 stations with the highest water levels
    highest = stations_highest_rel_level(stations, 5)

    # Creating dates for the past 10 days and fetching water levels
    dt = 10
    for h in highest:
        dates, levels = fetch_measure_levels(h.measure_id,
                                             dt=datetime.timedelta(days=dt))
        plot_water_levels(h, dates, levels)

if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")

    # Run Task2E
    run()
