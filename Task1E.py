from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number


def run():
    """Requirements for Task 1E"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))

    N = 9
    no_stations_rivers = rivers_by_station_number(stations, N)
    print(no_stations_rivers)


if __name__ == "__main__":
    print("*** Task 1E: CUED Part IA Flood Warning System ***")

    # Run Task1E
    run()
