from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations


def run():
    """Requirements for Task 1F"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))

    inconsistent_data = inconsistent_typical_range_stations(stations)
    inconsistent_data.sort()
    print('Stations with Inconsistent Data:', inconsistent_data)

if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")

    # Run Task1F
    run()
