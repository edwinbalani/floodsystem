"""Unit test for floodsystem.station"""

import floodsystem.station


def test_create_monitoring_station():
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = floodsystem.station.MonitoringStation(s_id, m_id, label,
                                              coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town


def test_typical_range_consistent():
    """Test to see if inconsistent data is found correctly"""
    # Create 2 stations with two different data ranges
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange1 = (-2.3, 3.4445)
    trange2 = (4.5, 1.7)
    river = "River X"
    town = "My Town"

    s1 = floodsystem.station.MonitoringStation(s_id, m_id, label, coord,
                                               trange1, river, town)
    s2 = floodsystem.station.MonitoringStation(s_id, m_id, label, coord,
                                               trange2, river, town)

    consistent1 = floodsystem.station.MonitoringStation \
                                     .typical_range_consistent(s1)
    consistent2 = floodsystem.station.MonitoringStation \
                                     .typical_range_consistent(s2)

    assert consistent1 is True
    assert consistent2 is False


def test_inconsistent_typical_range_stations():
    """Test to see if stations with inconsistent data are printed correctly"""
    # Create 2 stations with two different data ranges
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange1 = (-2.3, 3.4445)
    trange2 = (4.5, 1.7)
    river = "River X"
    town = "My Town"

    s1 = floodsystem.station.MonitoringStation(s_id, m_id, label, coord,
                                               trange1, river, town)
    s2 = floodsystem.station.MonitoringStation(s_id, m_id, label, coord,
                                               trange2, river, town)

    stations = [s1, s2]
    mylist = floodsystem.station.inconsistent_typical_range_stations(stations)

    assert mylist == [s2.name]
