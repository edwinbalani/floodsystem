from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance


def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))

    # Sort stations by distance from Cambridge city centre
    cam_cc = (52.2053, 0.1218)
    result = stations_by_distance(stations, cam_cc)

    # Print closest 10 (name, town, distance) tuples
    print([(s.name, s.town, d) for s, d in result[:10]])

    # Print furthest 10 (name, town, distance) tuples
    print([(s.name, s.town, d) for s, d in result[-10:]])


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")

    # Run Task1B
    run()
