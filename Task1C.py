from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius


def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))

    # Get stations within 10 km of Cambridge city centre
    cam_cc = (52.2053, 0.1218)
    result = stations_within_radius(stations, cam_cc, 10)

    # Print alphabetically sorted list of station names
    print(sorted([s.name for s in result]))


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")

    # Run Task1C
    run()
