from floodsystem.stationdata import build_station_list


def run():
    """Requirements for Task XX"""  # TODO change

    # Build list of stations
    stations = build_station_list()
    print("{} stations loaded.".format(len(stations)))

    # Demonstration code goes here

if __name__ == "__main__":
    print("*** Task XX: CUED Part IA Flood Warning System ***")  # TODO change

    # Run TaskXX
    run()
