"""This module implements functions for geographical calculations"""

from haversine import haversine
# from math import degrees
from .utils import sorted_by_key

AVG_EARTH_RADIUS = 6371  # kilometres


def stations_by_distance(stations, p):
    """Return stations sorted by distance from p"""
    return sorted_by_key([(station, haversine(station.coord, p))
                          for station in stations], 1)


def stations_within_radius(stations, centre, r):
    """Return stations within radius r of the centre point"""
    #    # Extracting coordinates
    #    lat_centre, lon_centre = centre

    #   # Create copy of stations list to avoid clobbering the original
    #   stations = stations[:]

    # BROKEN CODE - DO NOT UNCOMMENT
    #    # Filter stations not in a square bounding box
    #    angle = degrees(r / AVG_EARTH_RADIUS)
    #    stations = [s for s in stations
    #                if (abs(s.coord[0]-lat_centre) <= angle and
    #                    abs(s.coord[1]-lon_centre) <= angle)]
    #    # Discard remaining few
    return [s for s in stations if haversine(s.coord, centre) <= r]


# def find_bounding_box(p, radius):
#     lat, lon = p
#     angle = degrees(radius / AVG_EARTH_RADIUS)
#     return ((lat+angle, lon-angle),
#             (lat+angle, lon+angle),
#             (lat-angle, lon-angle),
#             (lat+angle, lon+angle))


def rivers_with_station(stations):
    """Return names of rivers with monitoring stations"""
#    # Create an empty set to store the rivers
#    r = set()
#    # Iterate over the stations to add their corresponding rivers
#    for station in stations:
#        r.add(station.river)
#    return r
    return {s.river for s in stations}


def stations_by_river(stations):
    """Return dictionary mapping rivers to their stations"""
#    # Empty dictionary to store all the rivers as keys
#    d = {}
#    # Iterate over stations to check if corresponding river is in d
#    for station in stations:
#        if station.river in d:
#            # Append the station as a value if its river is already a key
#            d[station.river].append(station)
#        else:
#            # Add the river as a key and then append the station as a value
#            d[station.river] = []
#            d[station.river].append(station)
#    return d
    return {r: [s for s in stations if s.river == r]
            for r in rivers_with_station(stations)}


def rivers_by_station_number(stations, N):
    """
    Find top N rivers with the most monitoring stations.

    May return more than N rivers if other rivers have the same number of
    stations as the Nth river.
    """
    rivers = stations_by_river(stations)

    # List to create and store tuples showing the number of stations
    # on each river in decreasing order of number of stations
    list_rivers = sorted([(r, len(rivers[r])) for r in rivers.keys()],
                         key=lambda x: x[1], reverse=True)

    # Empty list to store rivers with greatest number of rivers
    top_N = []
    nth = list_rivers[N-1][1]
    # Start counter
    i = 0
    # Add tuples to top_N when its 2nd value equals that of nth term in top_N
    while (i < len(list_rivers) and list_rivers[i][1] >= nth):
        top_N.append(list_rivers[i])
        i += 1
    return top_N
