"""This module contains functions related to the analysis of water levels."""
import numpy as np
import matplotlib


def polyfit(dates, levels, p):
    # Create lists for data
    # x values are given as the number of days since the year 0001
    x = matplotlib.dates.date2num(dates)
    y = levels

    # Check data consistency
    if not len(x) == len(y):
        raise ValueError('Lengths of date list and levels list do not match')

    # Check if there is data supplied, if not do nothing
    if len(x) == 0 or len(y) == 0:
        pass

    else:
        # Using shifted x values, find coefficient of best-fit
        # polynomial f(x) of degree p
        d0 = x - x[0]
        p_coeff = np.polyfit(d0, y, p)

        # Convert coefficient into a polynomial that can be evaluated
        # e.g. poly(0.3)
        poly = np.poly1d(p_coeff)

    try:
        return poly, d0
    except:
        pass
