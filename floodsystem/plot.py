"""This module implements functions for graphical plots"""
import matplotlib.pyplot as plt
from .analysis import polyfit
import numpy as np


def plot_water_levels(station, dates, levels):
    """
    Function which plots water level data against time for a station
    It includes plot lines for typical low and high data
    """
    # Check data consistency
    if not len(dates) == len(levels):
        raise ValueError('Lengths of date list and levels list do not match')

    if len(dates) == 0 or len(levels) == 0:
        print("Not enough data available for {} from agency"
              .format(station.name))
    else:
        # Plot
        plt.plot(dates, levels)

        # Add axis labels, rotate date labels and add plot title
        plt.xlabel('Date')
        plt.ylabel('Water Level (m)')
        plt.xticks(rotation=45)
        plt.title(station.name)

        # Add plot lines for the typical low and high levels if consistent
        # typical level data exists
        if station.typical_range_consistent():
            plt.axhline(y=station.typical_range[0], color='r', linestyle='-',
                        label='Lowest Typical Value')
            plt.axhline(y=station.typical_range[1], color='k', linestyle='-',
                        label='Highest Typical Value')
        else:
            raise ValueError('Typical range data is inconsistent')

        # Display plot
        plt.legend()
        plt.tight_layout()  # Makes sure plot does not cut off date labels

    return plt.show()


def plot_water_level_with_fit(station, dates, levels, p):
    """
    Plots water level data and the best-fit polynomial on the same graph
    """
    if len(dates) == 0 or len(levels) == 0:
        print("Not enough data available for {} from agency"
              .format(station.name))

    else:
        # Add axis labels, rotate date labels and add plot title
        plt.xlabel('Date')
        plt.ylabel('Water Level (m)')
        plt.xticks(rotation=45)
        plt.title(station.name)

        poly, d0 = polyfit(dates, levels, p)

        # Plot original water level values
        plt.plot(d0, levels, label='Water Level')

        # Plot polynomial fit at 100 points along interval
        # (note that polynomial is evaluated using the shift x)
        x1 = np.linspace(d0[0], d0[-1], 100)
        plt.plot(x1, poly(x1 - d0[0]), label='Best Fit')

        plt.legend()
        plt.tight_layout()  # Makes sure plot does not cut off date labels

    return plt.show()
