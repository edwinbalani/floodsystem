"""This module contains functions related to flood risk assessment."""
from .utils import sorted_by_key


def stations_level_over_threshold(stations, tol):
    """
    Find the stations where relative water level is over tol.

    Returns a list of (station, rel_level) tuples sorted in
    descending order of rel_level.

    Stations without consistent typical range data are ignored.
    """
    return sorted_by_key([(s, s.relative_water_level()) for s in stations
                          if s.relative_water_level() is not None and
                          s.relative_water_level() > tol], 1, reverse=True)


def stations_highest_rel_level(stations, N):
    """
    Find the N stations with highest relative water levels.

    Returns a list of MonitoringStation objects sorted in
    descending order of relative water level.

    Stations without consistent typical range data are ignored.
    """
    return [i[0] for i in stations_level_over_threshold(stations, 0)[:N]]
