"""This module provides a model for a monitoring station, and tools
for checking station data

"""


class MonitoringStation:
    """This class represents a river level monitoring station"""

    def __init__(self, station_id, measure_id, label, coord, typical_range,
                 river, town):

        self.station_id = station_id
        self.measure_id = measure_id

        # Handle case of erroneous data where data system returns
        # '[label, label]' rather than 'label'
        self.name = label
        if isinstance(label, list):
            self.name = label[0]

        self.coord = coord
        self.typical_range = typical_range
        self.river = river
        self.town = town

        self.latest_level = None

    def __repr__(self):
        d = "Station name:     {}\n".format(self.name)
        d += "   id:            {}\n".format(self.station_id)
        d += " measure id: {}\n".format(self.measure_id)
        d += "   coordinate:    {}\n".format(self.coord)
        d += "   town:          {}\n".format(self.town)
        d += "   river:         {}\n".format(self.river)
        d += "   typical range: {}".format(self.typical_range)
        return d

    def typical_range_consistent(self):
        """
        Check if typical range data for the station is sane.

        Criteria are that typical range data exists, and lower bound is not
        less than upper bound.
        """
        if self.typical_range is None:
            return False
        elif self.typical_range[0] - self.typical_range[1] > 0:
            return False
        else:
            return True

    def relative_water_level(self):
        """
        Return current water level 'scaled' to typical range min/max values.

        An absolute water level equal to the typical minimum yields a
        relative level of 0. Similarly, the typical maximum yields 1.

        Returns None if typical range data or water level data is missing,
        or if typical range data is inconsistent.
        """
        if self.typical_range_consistent() and self.latest_level is not None:
            return ((self.latest_level - self.typical_range[0]) /
                    (self.typical_range[1] - self.typical_range[0]))
        else:
            return None


def inconsistent_typical_range_stations(stations):
    """List all stations with inconsistent typical range data"""
    # Empty list to store names of stations with inconsistent data
    inconsistent = []
    # Iterate over stations to check for their consistency
    for s in stations:
        if s.typical_range_consistent() is False:
            inconsistent.append(s.name)
        else:
            pass
    return inconsistent
